package my.home.test;

import java.io.File;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import junit.framework.Assert;
import my.home.sampleapplication.SampleMultithreadedApp;
import my.home.test.utils.TestUtils;

import org.junit.Test;


/**
 * Integration test for run time modification of sample application using load
 * time weaving provided by AspectJ Weaver.
 * 
 * @author Jan Uhlir
 */
public class Test2TraceMultithreadedApp {
	@SuppressWarnings("unused")
	private final Logger logger = Logger.getLogger(Test2TraceMultithreadedApp.class.getName());

	/**
	 * To test load time weaving (LTW) we need to fork a new process, start new
	 * JVM with appropriate command line arguments, capturing he stream piped
	 * from the standard output stream of the forked process.
	 * <p>
	 * Note: we really have to use <code>p.getInputStream()</code>, it is not a
	 * mistake. The output from forked process is the input stream, stream to be
	 * read, from the perspective of this test process. Hence we need to get
	 * input stream from the tested application process.
	 * <p>
	 * Note: there should be NO output to stdout. The full output is logged
	 * using 'fine' level. If you want to see full sample application output,
	 * set 'fine' logging level for this test class. There may be some logging
	 * messages from AspectJ weaving process of interest.
	 */
	@Test
	public void testedInterceptedApplicationShouldCreateExpectedFilesWithExpectedContent() throws Exception {
		String classpath = System.getProperty("java.class.path");
		String aspectjWeaverJarFullPath = TestUtils.getAspectjWeaverJarFullPath();
		File testDirectory = TestUtils.getTestCleanDirectory();
		ProcessBuilder testedAppBuilder = new ProcessBuilder(
				"java",
				"-cp", classpath,  // they MUST BE separated as two arguments, otherwise you get -cp is not recognised option -
				"-javaagent:"+aspectjWeaverJarFullPath,
				"-DtraceAgentOutputDir=" + testDirectory.getAbsolutePath(),
				"-Djava.util.logging.ConsoleHandler.level=FINE",
				SampleMultithreadedApp.class.getName()
				);
		testedAppBuilder.redirectErrorStream(true);
		Process testedApp = null;
		try {
			testedApp = testedAppBuilder.start();
			StringWriter capturedOutput = new StringWriter();
			TestUtils.dumpProcessOutputInParalel(testedApp.getInputStream(), TestUtils.writer(System.out), TestUtils.writer(capturedOutput));

			Assert.assertEquals("OK", TestUtils.sendCommand("ping"));
			Assert.assertEquals("isAppSuspended=true, isTracingActive=false", TestUtils.sendCommand("stat"));
			Assert.assertEquals("OK", TestUtils.sendCommand("start"));
			Assert.assertEquals("OK", TestUtils.sendCommand("resume"));

			int exitValue = TestUtils.waitForProcessExitValue(testedApp, 10, TimeUnit.SECONDS);
			Assert.assertEquals(0, exitValue);

			Assert.assertEquals("",
					TestUtils.filterLinesStartingWith("\\[(.*)\\] error", capturedOutput.toString()));  // that is how AspectJ reports weaving errors

			File[] files = testDirectory.listFiles();
			Assert.assertEquals(2, files.length);
			Arrays.sort(files);
			Assert.assertTrue(files[0].getName().endsWith(".c.traceagent.log"));
			Assert.assertEquals("" + 
					"my.home.sampleapplication.SampleMultithreadedApp\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$SlowWorker\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$Worker1\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$Worker2\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$Worker3\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$WorkerA\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$WorkerB\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$WorkerC",
					TestUtils.textFileToString(files[0]).trim());
			Assert.assertTrue(files[1].getName().endsWith(".cm.traceagent.log"));
			Assert.assertEquals("" +
					"my.home.sampleapplication.SampleMultithreadedApp#main(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker#methodA(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker#methodB(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker#methodC(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$SlowWorker#methodZZZ(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$SlowWorker#run(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$Worker1#run(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$Worker2#run(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$Worker3#run(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$WorkerA#run(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$WorkerB#run(..)\n" +
					"my.home.sampleapplication.SampleMultithreadedApp$WorkerC#run(..)",
					TestUtils.textFileToString(files[1]).trim());
		} finally {
			if (testedApp != null) testedApp.destroy();
		}
	}
}
