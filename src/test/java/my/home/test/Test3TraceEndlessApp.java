package my.home.test;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.logging.Logger;

import junit.framework.Assert;
import my.home.sampleapplication.SampleEndlessApp;
import my.home.test.utils.TestUtils;

import org.junit.Test;


/**
 * Integration test for run time modification of sample application using load
 * time weaving provided by AspectJ Weaver.
 * 
 * @author Jan Uhlir
 */
public class Test3TraceEndlessApp {
	@SuppressWarnings("unused")
	private final Logger logger = Logger.getLogger(Test3TraceEndlessApp.class.getName());

	/**
	 * To test load time weaving (LTW) we need to fork a new process, start new
	 * JVM with appropriate command line arguments, capturing he stream piped
	 * from the standard output stream of the forked process.
	 * <p>
	 * Note: we really have to use <code>p.getInputStream()</code>, it is not a
	 * mistake. The output from forked process is the input stream, stream to be
	 * read, from the perspective of this test process. Hence we need to get
	 * input stream from the tested application process.
	 * <p>
	 * Note: there should be NO output to stdout. The full output is logged
	 * using 'fine' level. If you want to see full sample application output,
	 * set 'fine' logging level for this test class. There may be some logging
	 * messages from AspectJ weaving process of interest.
	 */
	@Test
	public void testedInterceptedApplicationShouldCreateExpectedFilesWithExpectedContentWhenSwitchingTracingOnAndOff() throws Exception {
		String classpath = System.getProperty("java.class.path");
		String aspectjWeaverJarFullPath = TestUtils.getAspectjWeaverJarFullPath();
		File testDirectory = TestUtils.getTestCleanDirectory();
		ProcessBuilder testedAppBuilder = new ProcessBuilder(
				"java",
				"-cp", classpath,  // they MUST BE separated as two arguments, otherwise you get -cp is not recognised option -
				"-javaagent:"+aspectjWeaverJarFullPath,
				"-DtraceAgentOutputDir=" + testDirectory.getAbsolutePath(),
				"-Djava.util.logging.ConsoleHandler.level=FINE",
				SampleEndlessApp.class.getName()
				);
		testedAppBuilder.redirectErrorStream(true);
		Process testedApp = null;
		try {
			testedApp = testedAppBuilder.start();
			StringWriter capturedOutput = new StringWriter();
			TestUtils.dumpProcessOutputInParalel(testedApp.getInputStream(), TestUtils.writer(System.out), TestUtils.writer(capturedOutput));

			Assert.assertEquals("OK", TestUtils.sendCommand("ping"));
			Assert.assertEquals("isAppSuspended=true, isTracingActive=false", TestUtils.sendCommand("stat"));
			Assert.assertEquals("OK", TestUtils.sendCommand("resume"));
			
			TestUtils.sleep(1000);
			
			// flush traces
			// since tracing is not activated, the contents must be empty
			Assert.assertEquals("OK", TestUtils.sendCommand("print"));
			testFileContents(testDirectory, "", "");
			
			// switch tracing ON and do some readings
			Assert.assertEquals("OK", TestUtils.sendCommand("start"));
			TestUtils.sleep(1000);
			
			// flush traces again (second attempt)
			// since tracing WAS activated, the contents must NOT be empty
			Assert.assertEquals("OK", TestUtils.sendCommand("print"));
			Assert.assertEquals("isAppSuspended=false, isTracingActive=true", TestUtils.sendCommand("stat"));
			testFileContents(testDirectory, 
					"my.home.sampleapplication.SampleEndlessApp", 
					"my.home.sampleapplication.SampleEndlessApp#printSomething(..)");
			
			// the application is endless, so we have to terminate is forcefully
			// should be the Socket Listener stopped explicitly first? should "stop" command be issued?
			Assert.assertEquals("OK", TestUtils.sendCommand("stop"));
		} finally {
			if (testedApp != null) testedApp.destroy();
		}
	}

	/**
	 * 
	 * Delete files after their content is fetched, to keep always minimum files in the tested temp directory.
	 * 
	 * @param testDirectory
	 * @param expectedContent1
	 * @param expectedContent2
	 * @throws IOException
	 */
	private void testFileContents(File testDirectory, String expectedContent1, String expectedContent2) throws IOException {
		File[] files = testDirectory.listFiles();
		Assert.assertEquals(2, files.length);
		Arrays.sort(files);
		Assert.assertTrue(files[0].getName().endsWith(".c.traceagent.log"));
		Assert.assertEquals(expectedContent1, 
				TestUtils.textFileToString(files[0]).trim());
		Assert.assertTrue(files[1].getName().endsWith(".cm.traceagent.log"));
		Assert.assertEquals(expectedContent2, 
				TestUtils.textFileToString(files[1]).trim());
		files[0].delete();
		files[1].delete();
	}
}
