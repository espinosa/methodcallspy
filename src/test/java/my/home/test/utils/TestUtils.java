package my.home.test.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import my.home.test.Test1TraceSimpleApp;

/**
 * Assorted set of static methods used in tests in this project. Many of them
 * are already provided by various frameworks and libraries but I wanted to keep
 * this project on Core Java level.
 * 
 * @author Jan Uhlir
 */
public class TestUtils {
	
	/**
	 * Try to find aspectj jar assuming it is in the classpath and current class
	 * loader is aware of aspectj classes. Run under Eclipse or Maven should
	 * honour such assumption. This implementation should deal well with
	 * pitfalls like multiple aspectj JARs in the classpath or unusual JAR
	 * name.
	 * 
	 * @param classpath
	 * @return full path of aspectj jar
	 */
	public static String getAspectjWeaverJarFullPath() {
		ClassLoader loader = Test1TraceSimpleApp.class.getClassLoader();
		String aspectjTypicalClass = "org/aspectj/weaver/ltw/LTWeaver.class";
		String aspectjUrl = loader.getResource(aspectjTypicalClass).toExternalForm();
		return aspectjUrl.substring(aspectjUrl.indexOf('/') + 1, aspectjUrl.indexOf(".jar!") + 4);
	}

	/**
	 * Return directory where class file is located for given Class.
	 * @param clazz
	 * @return
	 */
	public static String getClassFullDirectory(Class<?> clazz) {
		ClassLoader loader = clazz.getClassLoader();
		String classFileUrl = loader.getResource(clazz.getName().replace(".", "/") + ".class").toExternalForm();
		return classFileUrl.substring(classFileUrl.indexOf('/') + 1, classFileUrl.lastIndexOf("/"));
	}


	/**
	 * Pipe given input stream to standard output stream. Useful to dump
	 * output from a forked process.
	 * <p>
	 * This method blocks current thread waiting from output of external process.
	 */
	public static void dumpProcessOutputToStdout(InputStream is) throws IOException {
		BufferedReader reader = new BufferedReader (new InputStreamReader(is));
		String line;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
	}

	/**
	 * Pipe given input stream to standard output stream. Useful to dump
	 * output from a forked process.
	 * <p>
	 * This method blocks current thread waiting from output of external process.
	 */
	public static void dumpProcessOutputToLogger(InputStream is, Logger logger) throws IOException {
		BufferedReader reader = new BufferedReader (new InputStreamReader(is));
		String line;
		while ((line = reader.readLine()) != null) {
			logger.fine(line);
		}
	}


	/**
	 * Reprint given input stream to standard output stream. Useful to dump
	 * output from a forked process.
	 */
	public static String dumpProcessOutputToString(InputStream is) throws IOException {
		StringWriter sw = new StringWriter();
		PrintWriter out = new PrintWriter(sw);
		BufferedReader reader = new BufferedReader (new InputStreamReader(is));
		String line;
		while ((line = reader.readLine()) != null) {
			out.println(line);
		}
		return sw.toString();
	}

	/**
	 * Print continually input to all specified outputs in parallel thread.
	 * @param is
	 */
	public static void dumpProcessOutputInParalel(final InputStream is, final WriterAdapter... writers) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				BufferedReader reader = new BufferedReader (new InputStreamReader(is));
				String line;
				try {
					while ((line = reader.readLine()) != null) {
						for (WriterAdapter writer : writers) {
							writer.println(line);
						}
					}
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}).start();
	}

	/**
	 * Adapter to various output methods - logger, System.out, StringWriter, ..<br>
	 * Encapsulate classes like {@link PrintWriter} or {@link PrintStream}.<br>
	 * Provide uniform println() method.
	 */
	public static interface WriterAdapter {
		public void println(String s);
	}

	/**
	 * Static factory methods providing adapter of {@link WriterAdapter} for {@link PrintWriter}
	 */
	public static WriterAdapter writer(PrintWriter pw) {
		return new WriterAdapterForPrintWriter(pw);
	}
	
	/**
	 * Static factory methods providing adapter of {@link WriterAdapter} for {@link Writer}
	 */
	public static WriterAdapter writer(Writer w) {
		return new WriterAdapterForPrintWriter(new PrintWriter(w));
	}
	
	/**
	 * Static factory methods providing adapter of {@link WriterAdapter} for {@link PrintStream}
	 */
	public static WriterAdapter writer(PrintStream ps) {
		return new WriterAdapterForPrintStream(ps);
	}


	/**
	 * Encapsulate classes like {@link PrintWriter} or {@link PrintStream}.
	 * Provide uniform println() method.
	 */
	public static class WriterAdapterForPrintWriter implements WriterAdapter {
		PrintWriter pw;
		public WriterAdapterForPrintWriter(PrintWriter pw) {
			this.pw = pw;
		}
		public void println(String s) {
			pw.println(s);
		}
	}

	/**
	 * Encapsulate classes like {@link PrintWriter} or {@link PrintStream}.
	 * Provide uniform println() method.
	 */
	public static class WriterAdapterForPrintStream implements WriterAdapter {
		PrintStream ps;
		public WriterAdapterForPrintStream(PrintStream ps) {
			this.ps = ps;
		}
		public void println(String s) {
			ps.println(s);
		}
	}

	/**
	 * Define port number on which socket server runs in all local tests
	 */
	public static int SOCKET_PORT = 4444;

	/**
	 * Send command to a local socket server listening on {@link #SOCKET_PORT}.
	 * Client end of our IPC client server communication.
	 * 
	 * @param command
	 *            one line is expected. Example: "start" or "print"
	 * @return response from server
	 */
	public static String sendCommand(String command) throws IOException {
		Socket s = new Socket(InetAddress.getLocalHost(), SOCKET_PORT);
		PrintWriter out = new PrintWriter(s.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		out.println(command);
		String result = in.readLine();
		in.close();
		out.close();
		s.close();
		return result;
	}


	/**
	 * Return only lines with given prefix from given text.
	 * 
	 * @param startWith
	 *            filtering string, must be compatible with regular expressions,
	 *            some characters may require 'escaping'
	 * @param inputText
	 *            expected multi line text, text to filter
	 * @return only lines starting with given prefix
	 */
	public static String filterLinesStartingWith(String startWith, String inputText) {
		StringBuilder sb = new StringBuilder();
		Pattern p = Pattern.compile("^" + startWith);
		BufferedReader sr = new BufferedReader(new StringReader(inputText));
		String line;
		try {
			while ((line = sr.readLine()) != null) {
				Matcher m = p.matcher(line);
				if (m.find()) {
					if (sb.length()>0) {
						sb.append("\n");
					}
					sb.append(line);
				}
			}
		} catch (IOException e) {
			// should never get here, it is a StringReader! memory only operations
			throw new RuntimeException(e);
		}
		return sb.toString();
	}

	/**
	 * Run given task with a timeout, time restricted execution, if task does
	 * not finish correctly in given time, throw exception
	 * 
	 * @param timeout
	 * @param timeoutUnit
	 * @param task
	 *            runnable with time restricted execution
	 * @throws InterruptedException
	 */
	public static void runWithTimeout(long timeout, TimeUnit timeoutUnit, final Runnable task) 
			throws InterruptedException, TimeoutException {
		final AtomicBoolean finished = new AtomicBoolean(false);
		final Lock lock = new ReentrantLock();
		final Condition condition = lock.newCondition();
		new Thread(new Runnable() {
			@Override
			public void run() {
				task.run();
				try {
					lock.lock();
					finished.set(true);
					condition.signal();
				} finally {
					lock.unlock();
				}		
			}
		}).start();
		try {
			lock.lock();
			condition.await(timeout, timeoutUnit);
		} catch (InterruptedException ie) {
			Thread.currentThread().interrupt();
		} finally {
			lock.unlock();
		}
		if (!finished.get()) {
			throw new TimeoutException("Task did not finished in the given time");
		}
	}

	/**
	 * Wait for forked application, triggered in the test, encapsulated in
	 * {@link Process} to end. Capture exit value. Timeout waiting, so test are
	 * not blocked for ever.
	 * 
	 * @param p
	 *            encapsulate process, usually an external application
	 * @param timeout
	 * @param timeoutUnit
	 * @return return value of th eforked application
	 */
	public static int waitForProcessExitValue(final Process p, final int timeout, final TimeUnit timeoutUnit) 
			throws InterruptedException, TimeoutException {
		final AtomicInteger exitValue = new AtomicInteger();
		runWithTimeout(timeout, timeoutUnit, new Runnable() {
			@Override
			public void run() {
				try {
					exitValue.set(p.waitFor());
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});
		return exitValue.get();
	}

	/**
	 * Simple exception class to mark that task was finished due to timeout and not naturally
	 */
	public static class TimeoutException extends Exception {
		private static final long serialVersionUID = 1L;
		public TimeoutException(String message) {
			super(message);
		}
	}

	/**
	 * Create new test directory with no files
	 * Intended use is for testing ony.
	 * @return
	 * @throws IOException
	 */
	public static File getTestCleanDirectory() throws IOException {
	    final File tempDir = File.createTempFile("temp", Long.toString(System.nanoTime()));

	    if (tempDir.isFile() && tempDir.exists()) {
	    	if (!tempDir.delete()) {
	    		throw new IOException("Could not delete temp file: " + tempDir.getAbsolutePath());
	    	}
	    }

	    if (!tempDir.mkdir()) {
	        throw new IOException("Could not create temp directory: " + tempDir.getAbsolutePath());
	    }

	    return (tempDir);
	}

	/**
	 * Return content of a file as a byte array
	 */
	public static byte[] fileToString(File file) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buff = new byte[1024];
		int bytesRead = -1;
		BufferedInputStream fis = new BufferedInputStream(new FileInputStream(file));
		try {
			while ((bytesRead = fis.read(buff)) != -1) {
				out.write(buff, 0, bytesRead);
			}
		} finally {
			if (fis != null) fis.close();
		}
		return out.toByteArray();
	}
	
	/**
	 * Return content of a file as a string
	 */
	public static String textFileToString(File file) throws IOException {
		StringBuilder sb = new StringBuilder();
		String line;
		BufferedReader fis = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		try {
			while ((line = fis.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
		} finally {
			if (fis != null) fis.close();
		}
		return sb.toString();
	}
	
	/**
	 * Sleep given time in MS. Convenience variant of {@link Thread#sleep(long)}.
	 * Handle {@link InterruptedException}.
	 * @param timeMilis
	 */
	public static void sleep(long timeMilis) {
		try {
			Thread.sleep(timeMilis);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}