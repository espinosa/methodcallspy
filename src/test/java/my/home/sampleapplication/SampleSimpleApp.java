package my.home.sampleapplication;

/**
 * Dummy application used in integration tests. Really simple single thread
 * application.
 * 
 * @author Jan Uhlir
 */
public class SampleSimpleApp {

	public SampleSimpleApp() {
		System.out.println("inside constructor");
	}
	
	public void somePublicMethod() {
		System.out.println("inside somePublicMethod");
		somePrivateMethod();
	}

	private void somePrivateMethod() {
		System.out.println("inside somePrivateMethod");
	}

	public static void main(String[] args) {
		System.out.println("main() starts");
		SampleSimpleApp application = new SampleSimpleApp();
		application.somePublicMethod();
		System.out.println("main() ends");
	}
}
