package my.home.sampleapplication;

import java.util.concurrent.CountDownLatch;

/**
 * Dummy application used in integration tests. Multi thread application.
 * 
 * @author Jan Uhlir
 */
public class SampleMultithreadedApp {

	public static void main(String[] args) {
		new Thread(new WorkerA()).start();
		new Thread(new WorkerB()).start();
		new Thread(new WorkerC()).start();
		new Thread(new Worker1()).start();
		new Thread(new Worker2()).start();
		new Thread(new Worker3()).start();
		new Thread(new SlowWorker()).start();
		new UnusedClass();
		latch.countDown();
	}
	
	public static class AbstractBaseWorker implements BaseWorker {
		public void method0() {
			System.out.println(this.getClass().getName()+"::0");
		}
	}
	
	public interface BaseWorker {
		public void method0();
	}
	
	public interface IWorker {
		public void methodA();
		public void methodB();
		public void methodC();
	}
	
	public static class AbstractWorker implements IWorker {
		public void methodA() {
			System.out.println(this.getClass().getName()+"::A");
		}
		public void methodB() {
			System.out.println(this.getClass().getName()+"::B");
		}
		public void methodC() {
			System.out.println(this.getClass().getName()+"::C");
		}
	}

	public static final CountDownLatch latch = new CountDownLatch(1);

	public static class WorkerA extends AbstractWorker implements Runnable {
		@Override
		public void run() {
			try {
				latch.await();
				methodA();
				methodB();
				methodC();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public static class WorkerB extends AbstractWorker implements Runnable {
		@Override
		public void run() {
			try {
				latch.await();
				methodA();
				methodB();
				methodC();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public static class WorkerC extends AbstractWorker implements Runnable {
		@Override
		public void run() {
			try {
				latch.await();
				methodA();
				methodB();
				methodC();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public static class Worker1 extends AbstractWorker implements Runnable {
		@Override
		public void run() {
			try {
				latch.await();
				methodA();
				methodB();
				methodC();
				methodC();
				methodC();
				methodB();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public static class Worker2 extends AbstractWorker implements Runnable {
		@Override
		public void run() {
			try {
				latch.await();
				methodC();
				methodB();
				methodC();
				methodA();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public static class Worker3 extends AbstractWorker implements Runnable {
		@Override
		public void run() {
			try {
				latch.await();
				methodC();
				methodB();
				methodA();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}
	
	/**
	 * A thread intended to last longer then main() method,
	 * to test if flush() is called really when all threads finished.
	 */
	public static class SlowWorker implements Runnable {
		@Override
		public void run() {
			try {
				latch.await();
				Thread.sleep(1000);
				methodZZZ();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
		public void methodZZZ() {
			System.out.println(this.getClass().getName()+"::ZZZ");
		}
	}
	
	public static class UnusedClass {
		public void methodA() {
			System.out.println(this.getClass().getName()+"::UNUSED CLASS");
		}
	}
}
