package my.home.sampleapplication;

/**
 * Dummy application used in integration tests. Sample endless application,
 * endless loop, runs potentially forever.
 * 
 * @author Jan Uhlir
 */
public class SampleEndlessApp {

	public SampleEndlessApp() {
		System.out.println("inside constructor");
	}
	
	public void runEndlessLoop() {
		while (true) {
			printSomething();
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void printSomething() {
		//System.out.println("bla bla bla");
	}

	public static void main(String[] args) {
		SampleEndlessApp application = new SampleEndlessApp();
		application.runEndlessLoop();
	}
}
