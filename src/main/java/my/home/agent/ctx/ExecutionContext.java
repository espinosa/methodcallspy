package my.home.agent.ctx;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import my.home.agent.TraceSnapshotProvider;

/**
 * Encapsulate control of execution of intercepted application and tracing data
 * reading Manages socket server listener. Execute received commands.
 * <p>
 * Context instance is supposed to to be bootstrapped from JVM Agent when it is
 * initialised.
 * <p>
 * ExecutionContext is interface but contains factory singleton class
 * {@link Bootstrap}, see
 * {@link Bootstrap#instantiateDefaultContextClass(TraceSnapshotProvider)}
 * returning default implementation or instantiating provided class name.
 * 
 * @author Jan Uhlir
 */
public interface ExecutionContext {
	TraceSnapshotWriter getTraceWriter();
	

	/**
	 * Stop the whole context. Stop socket server. Called when intercepted
	 * application itself finishes or explicitly. Before stopping flush all
	 * trace data. There is no start() method since context is started
	 * automatically.
	 */
	void stop();
	
	/**
	 * Activate/Resume tracing of the intercepted application, activate/resume
	 * reading trace data
	 */
	void activateTracing();
	
	/**
	 * Stop tracing of the intercepted application, stop reading trace data 
	 */
	void deactivateTracing();
	
	/**
	 * Get tracing status.
	 * @return true if tracing of the intercepted application is active
	 */
	boolean isTracingActive();
	
	/**
	 * Flush gathered reading to an external system like
	 * file, permanent storage or distributed cache.
	 */
	void flushTrace();
	
	/**
	 * Resume intercepted application execution in case it was stopped.
	 * If it was running, do nothing.
	 */
    void resumeApp();
    
	/**
	 * Suspend (stop) intercepted application execution in case it was running.
	 * If it was already suspended, do nothing.
	 */
    void suspendApp();
    
    /**
     * Return execution status of intercepted application.
     * @return true if application is suspended
     */
    boolean isAppSuspended();
    
    /**
     * Stops intercepted application until "resume application" command is issued
     */
	void waitIfAppExecutionSuspended();
	
	/**
	 * Context bootstrapping class instance singleton
	 */
	final public Bootstrap BOOTSTRAP = new Bootstrap();
	
	/**
	 * System property name. If defined in system properties, custom execution
	 * context class is instantiated.
	 */
	final public String EXEC_CONTEXT_CLASS_SYSTEM_PROPERTY_NAME = "traceAgentExecutionContextClass";
	
	/**
	 * Context bootstrapping class implementation. Its
	 * {@link #get(TraceSnapshotProvider)} returns default implementation of
	 * {@link ExecutionContext} or instantiate custom implementation based on
	 * system property value.
	 */
	public static class Bootstrap {
		
		/**
		 * Factory method, instantiate and bootstrap execution context.
		 * Enable variable implementations, based on system property and default implementation if
		 * implementor was not specified. 
		 * @param traceSnapshotProvider context constructor parameter
		 * @return execution context instance
		 */
		public ExecutionContext get(TraceSnapshotProvider traceSnapshotProvider) {
			String execContextClass = System.getProperty(EXEC_CONTEXT_CLASS_SYSTEM_PROPERTY_NAME);
			if (execContextClass == null) {
				return instantiateDefaultContextClass(traceSnapshotProvider);
			} else {
				return instantiateParametrizedContextClass(execContextClass, traceSnapshotProvider);
			}
		}
		
		/**
		 * Create default implementation of {@link ExecutionContext}. 
		 * @param traceSnapshotProvider context constructor parameter
		 * @return context instance
		 */
		protected ExecutionContext instantiateDefaultContextClass(TraceSnapshotProvider traceSnapshotProvider) {
			return new SocketListeningExecutionContext(traceSnapshotProvider);
		}

		/**
		 * Create {@link ExecutionContext} based on given implementation class name.
		 * @param className must implement {@link ExecutionContext} interface 
		 * @param traceSnapshotProvider context constructor parameter
		 * @return cotext instance
		 */
		protected ExecutionContext instantiateParametrizedContextClass(String className, TraceSnapshotProvider traceSnapshotProvider) {
			try {
				@SuppressWarnings("unchecked")
				Class<ExecutionContext> clazz = (Class<ExecutionContext>)Class.forName(className);
				Constructor<ExecutionContext> constructor = (Constructor<ExecutionContext>)clazz.getDeclaredConstructor(TraceSnapshotProvider.class);
				ExecutionContext ec = (ExecutionContext)constructor.newInstance(traceSnapshotProvider);
				return ec;
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			} catch (InstantiationException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			} catch (SecurityException e) {
				throw new RuntimeException(e);
			} catch (NoSuchMethodException e) {
				throw new RuntimeException(e);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (InvocationTargetException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
