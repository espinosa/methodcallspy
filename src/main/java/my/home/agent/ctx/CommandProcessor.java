package my.home.agent.ctx;

import java.util.logging.Logger;

/**
 * Execute given textual command
 * 
 * @author Jan Uhlir
 */
public class CommandProcessor {
	/**
	 * Execution context reference
	 */
	private final ExecutionContext ctx;
	
	private final Logger logger = Logger.getLogger("TraceAgent");

	/**
	 * Main constructor
	 * @param socketListeningServer
	 */
	public CommandProcessor(ExecutionContext ctx) {
		this.ctx = ctx;
	}

	/**
	 * Execute given textual command
	 * @param inputLine
	 * @return
	 */
	public String process(String inputLine) {
		String result = null;
		if (inputLine == null) {
		} else if (inputLine.equals("start")) {
			logger.fine(">>> start tracing");
			ctx.activateTracing();
			result = "OK";
		} else if (inputLine.equals("stop")) {
			logger.fine(">>> stop tracing");
			ctx.deactivateTracing();
			result = "OK";
		} else if (inputLine.equals("ping")) {
			logger.fine(">>> ping");
			result = "OK";
		} else if (inputLine.equals("print")) {
			logger.fine(">>> Print TraceSnapshot");
			ctx.flushTrace();
			result = "OK";
		} else if (inputLine.equals("stat")) {
			result = "isAppSuspended="
					+ ctx.isAppSuspended() + ", " + "isTracingActive=" 
					+ ctx.isTracingActive();
		} else if (inputLine.equals("resume")) {
			logger.fine(">>> resume application");
			ctx.resumeApp();
			result = "OK";
		} else if (inputLine.equals("suspend")) {
			logger.fine(">>> suspend application");
			ctx.suspendApp();
			result = "OK";
		}
		return result;
	}
}