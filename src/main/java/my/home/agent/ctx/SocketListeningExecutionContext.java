package my.home.agent.ctx;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import my.home.agent.TraceSnapshotProvider;
import my.home.agent.utils.GuardedBy;
import my.home.agent.utils.ThreadSafe;
import my.home.agent.utils.Utils;

/**
 * Default implementation of {@link ExecutionContext}. Set up socket listening
 * server on given port. Provides thread safe execution of all basic actions
 * from {@link ExecutionContext}
 * 
 * @author Jan Uhlir
 */
@ThreadSafe
public class SocketListeningExecutionContext implements ExecutionContext {
	
	private static final int DEFAULT_TRACE_AGENT_PORT = 4444;

	private final SocketListeningServer traceAgentSocketServerRunnable;
	
	private final TraceSnapshoFileWriter traceSnapshoWriter;
	
	private final TraceSnapshotProvider traceSnapshotProvider; 
	
	private final Logger logger = Logger.getLogger("TraceAgent");
	
	@GuardedBy("this")
	private boolean tracingActive = false;
	
	private Lock appSuspendLock = new ReentrantLock();
	private Condition appSuspendCondition = appSuspendLock.newCondition();
	
	@GuardedBy("appSuspendLock")
	private boolean appSuspended = true;
	
	/**
	 * Main constructor. Set up socket listening server. Port is taken from
	 * system properties or is defaulted to {@link #DEFAULT_TRACE_AGENT_PORT}.
	 * 
	 * @param traceSnapshotProvider usually bootstrapping Agent (AspectJ interceptor) itself
	 */
	public SocketListeningExecutionContext(TraceSnapshotProvider traceSnapshotProvider) {
		this.traceSnapshotProvider = traceSnapshotProvider;
		String traceAgentOutputDir = Utils.getRequiredSystemProperty("traceAgentOutputDir");
		int traceAgentPort = Utils.getSystemPropertyInt("traceAgentPort", DEFAULT_TRACE_AGENT_PORT);
		this.traceSnapshoWriter = new TraceSnapshoFileWriter(traceAgentOutputDir);
		traceAgentSocketServerRunnable = startSocketServer(traceAgentPort);
	}
	
	/**
	 * Start socket server helper method.
	 * <p>
	 * Start server thread as daemon thread. This is crucial, every traced
	 * application would be endless otherwise. Application ends when last
	 * non-daemon thread ends. This socket listening server is daemon so it does
	 * not not block eventual natural application end.
	 * 
	 * @param socketServerPort
	 * @return
	 */
	protected SocketListeningServer startSocketServer(int socketServerPort) {
		logger.fine(">>> starting socket server 1");
		SocketListeningServer traceAgentSocketServerRunnable = new SocketListeningServer(this, socketServerPort);
		Thread serverDaemonThread = new Thread(traceAgentSocketServerRunnable);
		serverDaemonThread.setDaemon(true);
		serverDaemonThread.start();
		logger.fine(">>> starting socket server 2");
		return traceAgentSocketServerRunnable;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() {
		traceAgentSocketServerRunnable.stop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void activateTracing() {
		tracingActive = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void deactivateTracing() {
		tracingActive = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isTracingActive() {
		return tracingActive;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void flushTrace() {
		traceSnapshotProvider.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resumeApp() {
		try {
			appSuspendLock.lock();
			logger.fine(">>> resume");
			appSuspended = false;
			appSuspendCondition.signalAll();
		} finally {
			appSuspendLock.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void suspendApp() {
		try {
			appSuspendLock.lock();
			appSuspended = true;
		} finally {
			appSuspendLock.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isAppSuspended() {
		try {
			appSuspendLock.lock();
			return appSuspended;
		} finally {
			appSuspendLock.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void waitIfAppExecutionSuspended() {
		try {
			appSuspendLock.lock();
			while (appSuspended) { 
				logger.fine(">>> application halted, wait for resume");
				appSuspendCondition.await();
				if (!appSuspended) logger.fine(">>> application resumed");
			}
		} catch (InterruptedException ie) {
			Thread.currentThread().interrupt();
		} finally {
			appSuspendLock.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TraceSnapshotWriter getTraceWriter() {
		return traceSnapshoWriter;
	}
}
