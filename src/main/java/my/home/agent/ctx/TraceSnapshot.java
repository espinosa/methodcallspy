package my.home.agent.ctx;

import java.util.Set;
import java.util.TreeSet;

import my.home.agent.utils.ThreadSafe;

/**
 * Wrapping structure to gather trace data and provide ability to persist/flush
 * itself via delegation. Implementation of flush mechanism must be provided.
 * <p>
 * TraceSnapshot is shared between threads. It has to be thread safe.
 * 
 * @author Jan Uhlir
 */
@ThreadSafe
public class TraceSnapshot {
	
	final Set<String> classTrace;
	final Set<String> classAndMethodTrace;
	final TraceSnapshotWriter writer;
	
	public TraceSnapshot(TraceSnapshotWriter writer) {
		this.classTrace = new TreeSet<String>();
		this.classAndMethodTrace = new TreeSet<String>();
		this.writer = writer;
	}
	
	/**
	 * Add a trace record
	 * @param traceLine
	 */
	public synchronized void addClass(String traceLine) {
		classTrace.add(traceLine);
	}
	
	/**
	 * Add a trace record
	 * @param traceLine
	 */
	public synchronized void addClassAndMethod(String traceLine) {
		classAndMethodTrace.add(traceLine);
	}
	
	/**
	 * Write gathered data to a disk files	
	 */
	public void flush() {
		if (writer != null) {
			writer.flush(classTrace, classAndMethodTrace);
		}
	}
}