package my.home.agent.ctx;

import java.util.Set;

/**
 * Write gathered data to an external repository (output stream, file, IPC,
 * distributed cache)
 * 
 * @author Jan Uhlir
 */
public interface TraceSnapshotWriter {
	
	/**
	 * Write gathered data to an external repository (output stream, file, IPC,
	 * distributed cache)
	 */
	public void flush(Set<String> classTrace, Set<String> classAndMethodTrace);
}
