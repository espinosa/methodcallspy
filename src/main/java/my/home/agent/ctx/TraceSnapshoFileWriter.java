package my.home.agent.ctx;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

import my.home.agent.utils.Utils;

/**
 * Flush all gathered data to file
 * 
 * @author Jan Uhlir
 */
public class TraceSnapshoFileWriter extends TraceSnapshoStreamWriter {

	private final String outputDirectoryName;
	
	private final Logger logger = Logger.getLogger("TraceAgent");

	public TraceSnapshoFileWriter(String outputDirectoryName) {
		this.outputDirectoryName = outputDirectoryName;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected OutputStream getOutputStream(String fileFlag) throws IOException {
		File file = createUniqueTraceFileInOutputDirectory(fileFlag);
		return new FileOutputStream(file);
	}

	/**
	 * Create unique file for trace snapshot to be dumped there
	 */
	protected File createUniqueTraceFileInOutputDirectory(String fileFlag) throws IOException {
		File file = getOutputFile(Utils.timestamp(), fileFlag);
		try {
			file.createNewFile();
			return file;
		} catch (IOException e) {
			logger.fine("Cannot create file: " + file.getAbsolutePath() + "\n" + e.toString());
			throw e;
		}
	}
	
	/**
	 * Get output file name based on timestamp and suffix
	 * @param timestamp usually current time
	 * @param fileFlag file suffix
	 * @return
	 */
	protected File getOutputFile(String timestamp, String fileFlag) {
		String traceAgentOutputDir = getOutputDirectoryName();
		File dir = new File(traceAgentOutputDir);
		return new File(dir, timestamp + "." + fileFlag + ".traceagent.log");
	}

	public String getOutputDirectoryName() {
		return outputDirectoryName;
	}
}