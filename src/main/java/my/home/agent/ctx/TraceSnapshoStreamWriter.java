package my.home.agent.ctx;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Set;

/**
 * Flush all gathered data to an output stream. Two streams (files) are produced
 * - one is list of just classes, class names - classTrace; the other are method
 * calls - classAndMethodTrace. Every item is supposed to be listed only once.
 * 
 * @author Jan Uhlir
 */
public abstract class TraceSnapshoStreamWriter implements TraceSnapshotWriter {

	/**
	 * Write gathered data to an output stream (stream based repository like
	 * file system)
	 */
	@Override
	public synchronized void flush(Set<String> classTrace, Set<String> classAndMethodTrace) {
		try {
			printInternal(classTrace, getOutputStream("c"));
			printInternal(classAndMethodTrace, getOutputStream("cm"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Get output stream.
	 * 
	 * @param fileFlag
	 *            two streams (files) are produced, they are distinguished by
	 *            the file flag. example: "c" for classes only, "cm" for classes
	 *            and methods, will be part of the file name
	 */
	protected abstract OutputStream getOutputStream(String fileFlag) throws IOException;

	/**
	 * Dump gathered data to a disk file.
	 * 
	 * @param trace
	 *            gathered statistic
	 * @param out
	 *            output stream
	 * 
	 */
	protected void printInternal(Set<String> trace, OutputStream out) throws IOException {
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
		for (String line : trace) {
			bw.write(line);
			bw.newLine();
		}
		bw.flush();
		if (bw != null) out.close();
	}
}