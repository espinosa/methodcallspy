package my.home.agent.ctx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;


/**
 * Start server socket and wait for client commands until whole server is
 * stopped, internally or externally.
 * <p>
 * This is simplified implementation assuming only one client controlling the
 * tracing process. For our purpose this assumption is acceptable. Otherwise
 * client handling routine needs to be run in another thread(s).
 * 
 * @author Jan Uhlir
 */
public class SocketListeningServer implements Runnable {
	
	final ExecutionContext ctx;
	
	private final int socketServerPort;
	
	private volatile boolean stopFlag = false;

	private ServerSocket serverSocket = null;
	
	final CommandProcessor commandProcessor;
	
	private final Logger logger = Logger.getLogger("TraceAgent");

	/**
	 * Main constructor
	 * @param socketListeningTraceAllAgent
	 */
	public SocketListeningServer(ExecutionContext ctx, int socketServerPort) {
		this.socketServerPort = socketServerPort;
		this.ctx = ctx;
		this.commandProcessor = new CommandProcessor(ctx);
	}
	
	/**
	 * Start server socket and wait for client until whole server is stopped,
	 * internally or externally.
	 */
	public void run() {
		logger.fine(">>> try to start socket server listening on port 4444");
		try {
			serverSocket = new ServerSocket(getTraceAgentPort());
			logger.fine(">>> socket server started");			
			while (!stopFlag) {
				Socket clientSocket = waitForClient();
				if (clientSocket != null) {
					processClient(clientSocket);
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			try {
				if (serverSocket != null && !serverSocket.isClosed()) serverSocket.close();
			} catch (IOException e) {
				logger.fine(">>> AgentSocketServerRunnable IOException when closing a server socket : " + e.toString());
			}
		}
	}
	
	/**
	 * Get server socket port, port to communicate with trace agent.
	 * @return
	 */
	public int getTraceAgentPort() {
		return socketServerPort;
	}
	
	/**
	 * Wait for client attempt to connect
	 * 
	 * @return socket to client
	 */
	protected Socket waitForClient() {
		Socket clientSocket = null;
		try {
			clientSocket = serverSocket.accept();
			logger.fine(">>> client connection accepted");
		} catch (IOException e) {
			if (!stopFlag) logger.fine(">>> TraceAgentSocketServerRunnable: Accept failed");
		}
		return clientSocket;
	}

	/**
	 * Process client command
	 * <p>
	 * This is simplified implementation assuming only one client controlling
	 * the tracing process. For our purpose this assumption is acceptable.
	 * Otherwise this method needs to be run in another thread.
	 * 
	 * @param clientSocket
	 */
	protected void processClient(Socket clientSocket) {
		BufferedReader in = null;
		PrintWriter out = null;
		try {
			logger.fine(">>> opening streams");
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			logger.fine(">>> streams opened");
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				logger.fine(">>> line read " + inputLine);
				String result = commandProcessor.process(inputLine);
				if (result != null) out.println(result);
			}
			in.close();
			out.close();
			clientSocket.close();
		} catch (IOException e) {
			logger.fine(">>> AgentSocketServerRunnable: " + e.toString());
		}
	}
	
	/**
	 * Stop the whole context. Stop socket server. Called when intercepted
	 * application itself finishes or explicitly. Before stopping flush all
	 * trace data.
	 */
	public synchronized void stop() {
		ctx.flushTrace();
		stopFlag = true;
		try {
			if (serverSocket!=null && !serverSocket.isClosed()) serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}