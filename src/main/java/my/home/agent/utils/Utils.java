package my.home.agent.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
	/**
	 * Get output directory name. Expose this detail to be printed early in the process 
	 * @return
	 */
	public static String getRequiredSystemProperty(String propertyName) {
		String value = System.getProperty("traceAgentOutputDir", null);
		if (value==null) throw new RuntimeException("Property " + propertyName + " must be specified");
		return value;
	}
	
	public static int getSystemPropertyInt(String propertyName, int defaultValue) {
		String value = System.getProperty("traceAgentOutputDir");
		if (value==null) return Integer.parseInt(value);
		else return defaultValue;
	}
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd--HH-mm-sss");

	public static String timestamp() {
		return sdf.format(new Date());
	}
}
