package my.home.agent;

/**
 * Define all essential operations for class managing trace snapshot
 * 
 * @author Jan Uhlir
 */
public interface TraceSnapshotProvider {
	
	/**
	 * Flush snapshot, assuming some permanent storage, dumping to disk file.
	 */
	void flush();
}
